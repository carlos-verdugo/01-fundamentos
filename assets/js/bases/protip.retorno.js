const crearPersona = (nombre, apellido) => ({ nombre, apellido });

const persona = crearPersona('Carlos', 'Verdugo');

function imprimeArgumentos2() {
    console.log('Function', arguments)
}

imprimeArgumentos2(10, true, false, 'Fernando', 'Hola')

const imprimeArgumentos = (...args) => {
    console.log('Arrow', args);
    return args;
}

const argumento = imprimeArgumentos(10, true, false, 'Fernando', 'Hola');
console.log('Argumento', argumento);

const [casado, vivo, nombre, saludo] = imprimeArgumentos(10, true, false, 'Fernando', 'Hola')
console.log({casado, vivo, nombre, saludo});

// const {apellido} = crearPersona('Carlos', 'Verdugo');
const { apellido: nuevoApellido } = crearPersona('Carlos', 'Verdugo');
console.log({nuevoApellido});

const tony = {
    nombre: 'Tony Stark',
    codeName: 'Iron man',
    vivo: false,
    edad: 40,
    trajes: ['Mark I', 'Mark V', 'Hulkbuster'],
};

// const imprimePropiedades = (personaje) =>{
//     console.log(personaje.nombre);
//     console.log(personaje.codeName);
//     console.log(personaje.vivo);
//     console.log(personaje.edad);
//     console.log(personaje.trajes)
// }

const imprimePropiedades = ({nombre, codename = 0, vivo, edad}) =>{
    // codename = codename || 0;
    console.log({nombre});
    console.log({codename});
    console.log({vivo});
    console.log({edad});
}

imprimePropiedades(tony);