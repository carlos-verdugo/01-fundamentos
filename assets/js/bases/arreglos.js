// const arr = new Array(10);
// const arr = [];
// console.log(arr);

let videoJuegos = ['Mario 3', 'Megaman', 'Chrono Trigger'];
console.table({videoJuegos});

console.log(videoJuegos[0]);

let arregloCosas = [
    true,
    123,
    'Carlos',
    1 + 2,
    function() {},
    ()=>{},
    {a:1},
    ['X', 'Megaman', 'Zero', 'Dr. Light', [
        'Dr. Willy',
        'Woodman'
    ]]
];

console.table(arregloCosas)

console.log(arregloCosas[7][4][1])