let a = 11;

if ( a > 10 ) {
    console.log('A es mayor a 10');
} else {
    console.log('A es menor a 10');
}

console.log('Fin de programa')

const hoy = new Date();

console.log(hoy);

let dia = hoy.getDay();

console.log(dia);

if (dia === '2') {
    console.log('Es martes')
} else {
    console.log('No es martes')
}

// sin if else
dia = 2;

// const dias = {
//     0: 'domingo',
//     1: 'lunes',
//     2: 'martes',
//     3: 'miercoles'
// }

// console.log(dias[dia] || 'Día no definido')

const dias = {
    0: () => 'domingo',
    1: () => 'lunes',
    2: () => 'martes',
    3: () => 'miercoles'
}

console.log(dias[dia]() || 'Día no definido')