let nombre = 'Peter Parker';
console.log(nombre);

nombre = "Tía May";
nombre = `Tía May`;

console.log(typeof nombre);

nombre = 123;
console.log(typeof nombre);

let esMarvel = true;
console.log(typeof esMarvel);

let edad = 33;
console.log(typeof edad);

let superPoder;
console.log(typeof superPoder)

let soyNull = null;
console.log(soyNull);

let symbol = Symbol('a');
let symbol2 = Symbol('a');
console.log(typeof symbol);

console.log(symbol === symbol2);
